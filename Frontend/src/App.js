import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import ListStudentComponent from './components/ListStudentComponent';
import CreateStudentComponent from './components/CreateStudentComponent';
import ViewStudentComponent from './components/ViewStudentComponent';
import ListCourseComponent from './components/ListCourseComponent';
import CreateCourseComponent from './components/CreateCourseComponent';
import ViewCourseComponent from './components/ViewCourseComponent';
import ListRegistrationComponent from './components/ListRegistrationComponent';
import CreateRegistrationComponent from './components/CreateRegistrationComponent';

function App() {
  return (
    <div>

      <Router>

          <HeaderComponent />

            <div className="container">

              <Switch>
                
                <Route path = "/students" component = {ListStudentComponent}></Route>
                <Route path = "/add-student/:id" component = {CreateStudentComponent}></Route>
                <Route path = "/view-student/:id" component = {ViewStudentComponent}></Route>

                <Route path = "/courses" component = {ListCourseComponent}></Route>
                <Route path = "/add-course/:id" component = {CreateCourseComponent}></Route>
                <Route path = "/view-course/:id" component = {ViewCourseComponent}></Route>

                <Route path = "/registrations" component = {ListRegistrationComponent}></Route>
                <Route path = "/add-registration/:id" component = {CreateRegistrationComponent}></Route>
                
              </Switch>

            </div>

          <FooterComponent  />

      </Router>

    </div>
 
  );
}

export default App;

