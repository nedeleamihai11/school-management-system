import axios from 'axios'

const COURSE_API_BASE_URL = "http://localhost:8080/api/v1/courses";

class CourseService{

    getCourses(){
        return axios.get(COURSE_API_BASE_URL);
    }

    createCourse(course){
        return axios.post(COURSE_API_BASE_URL, course)
    }

    getCourseById(courseID){
        return axios.get(COURSE_API_BASE_URL + '/' + courseID);
    }

    updateCourse(course, courseID){
        return axios.put(COURSE_API_BASE_URL + '/' + courseID, course);
    }

    deleteCourse(courseID){
        return axios.delete(COURSE_API_BASE_URL + '/' + courseID);
    }
}

export default new CourseService()