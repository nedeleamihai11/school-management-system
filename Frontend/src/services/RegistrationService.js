import axios from 'axios'

const REGISTRATION_API_BASE_URL = "http://localhost:8080/api/v1/registrations";

class RegistrationService{

    getRegistrations(){
        return axios.get(REGISTRATION_API_BASE_URL);
    }

    createRegistrations(registration){
        return axios.post(REGISTRATION_API_BASE_URL, registration)
    }

    addStudentToRegistration(registrationID, student, studentID){
        return axios.put(REGISTRATION_API_BASE_URL + '/' + registrationID + '/student/' + studentID, student);
    }

    addCourseToRegistration(registrationID, course, courseID){
        return axios.put(REGISTRATION_API_BASE_URL + '/' + registrationID + '/course/' + courseID, course);
    }
}

export default new RegistrationService()