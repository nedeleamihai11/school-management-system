import React, { Component } from 'react';
import CourseService from '../services/CourseService';

class CreateCourseComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            id: this.props.match.params.id,
            name: '',
            duration: '',
            profesor_name: '',
            credit_points: ''
            
        }

        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.changeDurationHandler = this.changeDurationHandler.bind(this);
        this.changeProfesorNameHandler = this.changeProfesorNameHandler.bind(this);
        this.changeCreditPointsHandler = this.changeCreditPointsHandler.bind(this);
    }

    //Step 3
    componentDidMount(){

        //Step 4
        if(this.state.id === '_add'){
            return;
        
        }else{

            CourseService.getCourseById(this.state.id).then( (res) => {
                let course = res.data;
                this.setState({name: course.name,
                                duration: course.duration,
                                profesor_name: course.profesor_name,
                                credit_points: course.credit_points
                });
            });
        }
    }

    SaveOrUpdateCourse = (e) => {
        e.preventDefault();
        let course = {name: this.state.name,
                        duration: this.state.duration,
                        profesor_name: this.state.profesor_name,
                        credit_points: this.state.credit_points,
                        };
        console.log('course =>' + JSON.stringify(course));

        //Step 5
        if (this.state.id === "_add"){

            CourseService.createCourse(course).then(res => {
                this.props.history.push('/courses');
            });

        }else{

            CourseService.updateCourse(course, this.state.id).then( res => {
                this.props.history.push('/courses');
            });
        }        
    } 

    changeNameHandler = (event) => {
        this.setState({name: event.target.value});
    }

    changeDurationHandler = (event) => {
        this.setState({duration: event.target.value});
    }

    changeProfesorNameHandler = (event) => {
        this.setState({profesor_name: event.target.value});
    }

    changeCreditPointsHandler = (event) => {
        this.setState({credit_points: event.target.value});
    }

    cancel(){
        this.props.history.push('/courses');
    }

    getTitle(){
        if(this.state.id === "_add"){
            return <h3 className = "text-center">Add Course</h3>
        } else {
            return <h3 className = "text-center">Update Course</h3>
        }
    }

    render() {
        return (
            <div>
                <div className = "container">
                    <br></br>
                    <div className = "row">
                        <div className = "card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className = "card-body">
                                <form>
                                    <div className = "form-group">
                                        <label> Name: </label>
                                        <input placeholder="Name" name="name" className="form-control"
                                        value={this.state.name} onChange={this.changeNameHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Duration: </label>
                                        <input placeholder="Duration" name="duration" className="form-control"
                                        value={this.state.duration} onChange={this.changeDurationHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Profesor Name: </label>
                                        <input placeholder="Profesor Name" name="profesor_name" className="form-control"
                                        value={this.state.profesor_name} onChange={this.changeProfesorNameHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Credit Points: </label>
                                        <input placeholder="Credit Points" name="credit_points" className="form-control"
                                        value={this.state.credit_points} onChange={this.changeCreditPointsHandler}/>
                                    </div>

                                    <br></br>
                                    <button className="btn btn-success" onClick={this.SaveOrUpdateCourse}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateCourseComponent;