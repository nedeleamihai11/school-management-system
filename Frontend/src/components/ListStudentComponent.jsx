import React, { Component } from 'react';
import StudentService from '../services/StudentService';
import RemoveRedEyeIcon from '@material-ui/icons/RemoveRedEye';
import TrashIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';

class ListStudentComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            students: []
        }

        this.addStudent = this.addStudent.bind(this);
        this.editStudent = this.editStudent.bind(this);
        this.deleteStudent = this.deleteStudent.bind(this);
        this.viewStudent = this.viewStudent.bind(this);

    }

    deleteStudent(id){
        StudentService.deleteStudent(id).then((res) => {
            this.setState({ students: this.state.students.filter(student => student.id !== id)});
        });
    }

    viewStudent(id){
        this.props.history.push(`/view-student/${id}`)
    }

    editStudent(id){
        this.props.history.push(`/add-student/${id}`);
    }

    /*Get data from Student*/
    componentDidMount(){
        StudentService.getStudents().then((res) => {
            this.setState({ students: res.data});
        });
    }

    addStudent(){
        this.props.history.push('/add-student/_add');

    }

    render() {
        return (
            <div>
                <h2 className="text-center">Students List</h2>
                <br></br>
                <div className = "row">
                    <table className = "table table-strip table-bordered">

                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Date Birth</th>
                                <th>CNP</th>
                                <th>Phone Number</th>
                                <th>Father Name</th>
                                <th>Mother Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.students.map(
                                    student => 
                                    <tr key ={student.id}>
                                        <td> {student.firstName}</td>
                                        <td> {student.lastName}</td>
                                        <td> {student.emaildID}</td>
                                        <td> {student.datebirth}</td>
                                        <td> {student.cnp}</td>
                                        <td> {student.phonenumber}</td>
                                        <td> {student.father_name}</td>
                                        <td> {student.mother_name}</td>
                                        <td>
                                            <button onClick = { () => this.editStudent(student.id)} className="btn btn-success">
                                            <EditIcon/>  
                                            </button>                                            
                                            
                                            <button style={{marginLeft: "10px"}} onClick = { () => this.viewStudent(student.id)} className="btn btn-warning">
                                            <RemoveRedEyeIcon/>
                                            </button>
                                            
                                            <button style={{marginLeft: "10px"}} onClick = { () => this.deleteStudent(student.id)} className="btn btn-danger">
                                            <TrashIcon/>
                                            </button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>

                <div className = "row" >
                    <button className="btn btn-primary" onClick={this.addStudent}>Add Student</button>
                </div>

            </div>
        );
    }
}

export default ListStudentComponent;