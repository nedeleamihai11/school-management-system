import React, { Component } from 'react';
import CourseService from '../services/CourseService';

class ViewCourseComponent extends Component {
    constructor(props){
        super(props);

        this.state = {
            id: this.props.match.params.id,
            course: {}
        }
    }

    componentDidMount(){
        CourseService.getCourseById(this.state.id).then( res => {
            this.setState({course: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Course Details</h3>
                    <div className = "card-body">
                       
                       <div className = "row">
                           <label> Name : {this.state.course.name}</label>
                       </div>

                       <div className = "row">
                           <label> Duration : {this.state.course.duration}</label>
                       </div>

                       <div className = "row">
                           <label> Profesor Name : {this.state.course.profesor_name}</label>
                       </div>

                       <div className = "row">
                           <label> Credit Points : {this.state.course.credit_points}</label>
                       </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ViewCourseComponent;