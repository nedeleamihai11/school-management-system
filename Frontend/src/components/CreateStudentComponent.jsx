import React, { Component } from 'react';
import StudentService from '../services/StudentService';

class CreateStudentComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            //Step 2
            id: this.props.match.params.id,
            firstName: '',
            lastName: '',
            emaildID: '',
            datebirth: '',
            phonenumber: '',
            mother_name: '',
            father_name: '',
            cnp: ''
            
        }

        this.changeFirstNameHandler = this.changeFirstNameHandler.bind(this);
        this.changeLastNameHandler = this.changeLastNameHandler.bind(this);
        this.changeEmailIDHandler = this.changeEmailIDHandler.bind(this);
        this.changeDateBirthHandler = this.changeDateBirthHandler.bind(this);
        this.changePhoneNumberHandler = this.changePhoneNumberHandler.bind(this);
        this.changeFatherNameHandler = this.changeFatherNameHandler.bind(this);
        this.changeMotherNameHandler = this.changeMotherNameHandler.bind(this);
        this.changeCNPHandler = this.changeCNPHandler.bind(this);
        this.SaveOrUpdateStudent = this.SaveOrUpdateStudent.bind(this);
    }

    //Step 3
    componentDidMount(){

        //Step 4
        if(this.state.id === '_add'){
            return;
        
        }else{

            StudentService.getStudentById(this.state.id).then( (res) => {
                let student = res.data;
                this.setState({firstName: student.firstName,
                                lastName: student.lastName,
                                emaildID: student.emaildID,
                                datebirth: student.datebirth,
                                phonenumber: student.phonenumber,
                                mother_name: student.mother_name,
                                father_name: student.father_name,
                                cnp: student.cnp
                });
            });
        }
    }

    SaveOrUpdateStudent = (e) => {
        e.preventDefault();
        let student = {firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        emaildID: this.state.emaildID,
                        datebirth: this.state.datebirth,
                        phonenumber: this.state.phonenumber,
                        mother_name: this.state.mother_name,
                        father_name: this.state.father_name,
                        cnp: this.state.cnp
                        };
        console.log('student =>' + JSON.stringify(student));

        //Step 5
        if (this.state.id === "_add"){

            StudentService.createStudent(student).then(res => {
                this.props.history.push('/students');
            });

        }else{

            StudentService.updateStudent(student, this.state.id).then( res => {
                this.props.history.push('/students');
            });
        }        
    } 

    changeFirstNameHandler = (event) => {
        this.setState({firstName: event.target.value});
    }

    changeLastNameHandler = (event) => {
        this.setState({lastName: event.target.value});
    }

    changeEmailIDHandler = (event) => {
        this.setState({emaildID: event.target.value});
    }

    changeDateBirthHandler = (event) => {
        this.setState({datebirth: event.target.value});
    }

    changePhoneNumberHandler = (event) => {
        this.setState({phonenumber: event.target.value});
    }

    changeFatherNameHandler = (event) => {
        this.setState({father_name: event.target.value});
    }

    changeMotherNameHandler = (event) => {
        this.setState({mother_name: event.target.value});
    }

    changeCNPHandler = (event) => {
        this.setState({cnp: event.target.value});
    }

    cancel(){
        this.props.history.push('/students');
    }

    getTitle(){
        if(this.state.id === "_add"){
            return <h3 className = "text-center">Add Student</h3>
        } else {
            return <h3 className = "text-center">Update Student</h3>
        }
    }

    render() {
        return (
            <div>
                <div className = "container">
                    <br></br>
                    <div className = "row">
                        <div className = "card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className = "card-body">
                                <form>
                                    <div className = "form-group">
                                        <label> First Name: </label>
                                        <input placeholder="First Name" name="firstname" className="form-control"
                                        value={this.state.firstName} onChange={this.changeFirstNameHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Last Name: </label>
                                        <input placeholder="Last Name" name="lastname" className="form-control"
                                        value={this.state.lastName} onChange={this.changeLastNameHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Email: </label>
                                        <input placeholder="Email" name="emaildID" className="form-control"
                                        value={this.state.emaildID} onChange={this.changeEmailIDHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Datebirth: </label>
                                        <input placeholder="yyyy-mm-dd" name="datebirth" className="form-control"
                                        value={this.state.datebirth} onChange={this.changeDateBirthHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Phone Number: </label>
                                        <input placeholder="Phonenumber" name="phonenumber" className="form-control"
                                        value={this.state.phonenumber} onChange={this.changePhoneNumberHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Mother Name: </label>
                                        <input placeholder="Mother Name" name="mother_name" className="form-control"
                                        value={this.state.mother_name} onChange={this.changeMotherNameHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Father Name: </label>
                                        <input placeholder="Father Name" name="father_name" className="form-control"
                                        value={this.state.father_name} onChange={this.changeFatherNameHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> CNP: </label>
                                        <input placeholder="CNP" name="cnp" className="form-control"
                                        value={this.state.cnp} onChange={this.changeCNPHandler}/>
                                    </div>

                                    <br></br>
                                    <button className="btn btn-success" onClick={this.SaveOrUpdateStudent}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateStudentComponent;