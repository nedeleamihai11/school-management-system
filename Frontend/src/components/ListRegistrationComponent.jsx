import React, { Component } from 'react';
import RegistrationService from '../services/RegistrationService';

class ListRegistrationComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            registrations: []
        }

        this.addRegistration = this.addRegistration.bind(this);

    }

    /*Get data from Registration*/
    componentDidMount(){
        RegistrationService.getRegistrations().then((res) => {
            this.setState({ registrations: res.data});
        });
    }

    addRegistration(){
        this.props.history.push('/add-registration/_add');

    }

    checkIsNull(property){
        if(property == null){
            return true;
        } else {
            return false;
        }
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Registrations List</h2>
                <br></br>
                <div className = "row">
                    <table className = "table table-strip table-bordered">

                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Course Name</th>
                                <th>Examen Grade</th>
                                <th>Project Grader</th>
                                <th>Laborator Grade</th>
                                <th>Presence Grade</th>
                                <th>Registration Date</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.registrations.map(
                                    registration => 
                                    <tr key ={registration.id}>
                                        <td> {this.checkIsNull(registration.student) ? 'N/A' : registration.student.firstName}</td>
                                        <td> {this.checkIsNull(registration.student) ? 'N/A' : registration.student.lastName}</td>
                                        <td> {this.checkIsNull(registration.course) ? 'N/A' : registration.course.name}</td>
                                        <td> {registration.examen_grade}</td>
                                        <td> {registration.project_grade}</td>
                                        <td> {registration.laborator_grade}</td>
                                        <td> {registration.presence_grade}</td>
                                        <td> {registration.registration_date}</td>

                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>

                <div className = "row" >
                    <button className="btn btn-primary" onClick={this.addRegistration}>Add Registration</button>
                </div>

            </div>
        );
    }
}

export default ListRegistrationComponent;