import React, { Component } from 'react';
import StudentService from '../services/StudentService';

class ViewStudentComponent extends Component {
    constructor(props){
        super(props);

        this.state = {
            id: this.props.match.params.id,
            student: {}
        }
    }

    componentDidMount(){
        StudentService.getStudentById(this.state.id).then( res => {
            this.setState({student: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Student Details</h3>
                    <div className = "card-body">
                       
                       <div className = "row">
                           <label> First Name : {this.state.student.firstName}</label>
                       </div>

                       <div className = "row">
                           <label> Last Name : {this.state.student.lastName}</label>
                       </div>

                       <div className = "row">
                           <label> Email : {this.state.student.emaildID}</label>
                       </div>

                       <div className = "row">
                           <label> Datebirth : {this.state.student.datebirth}</label>
                       </div>

                       <div className = "row">
                           <label> CNP : {this.state.student.cnp}</label>
                       </div>

                       <div className = "row">
                           <label> Phonenumber : {this.state.student.phonenumber}</label>
                       </div>

                       <div className = "row">
                           <label> Father Name : {this.state.student.father_name}</label>
                       </div>

                       <div className = "row">
                           <label> Mother Name : {this.state.student.mother_name}</label>
                       </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ViewStudentComponent;