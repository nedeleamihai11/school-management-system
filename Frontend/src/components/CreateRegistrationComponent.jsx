import React, { Component } from 'react';
import RegistrationService from '../services/RegistrationService';

class CreateRegistrationComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            id: this.props.match.params.id,
            firstName: 'N/A',
            lastName: 'N/A',
            courseName: 'N/A',
            registration_date: '',
            examen_grade: '',
            project_grade: '',
            laborator_grade: '',
            presence_grade: ''
            
        }

        //this.changeFirstNameHandler = this.changeFirstNameHandler.bind(this);
        //this.changeLastNameHandler = this.changeLastNameHandler.bind(this);
        //this.changecourseNameHandler = this.changecourseNameHandler.bind(this);
        this.changeregistration_dateHandler = this.changeregistration_dateHandler.bind(this);
        this.changeexamen_gradeHandler = this.changeexamen_gradeHandler.bind(this);
        this.changeproject_gradeHandler = this.changeproject_gradeHandler.bind(this);
        this.changelaborator_gradeHandler = this.changelaborator_gradeHandler.bind(this);
        this.changepresence_gradeHandler = this.changepresence_gradeHandler.bind(this);
    }

    //Step 3
    componentDidMount(){

        //Step 4
        if(this.state.id === '_add'){
            return;
        
        }else{

            /*CourseService.getCourseById(this.state.id).then( (res) => {
                let course = res.data;
                this.setState({name: course.name,
                                duration: course.duration,
                                profesor_name: course.profesor_name,
                                credit_points: course.credit_points
                });
            });*/
        }
    }

    SaveOrUpdateCourse = (e) => {
        e.preventDefault();
        let registration = {firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        courseName: this.state.courseName,
                        registration_date: this.state.registration_date,
                        examen_grade: this.state.examen_grade,
                        project_grade: this.state.project_grade,
                        laborator_grade: this.state.laborator_grade,
                        presence_grade: this.state.presence_grade
                        };
        console.log('registration =>' + JSON.stringify(registration));

        //Step 5
        if (this.state.id === "_add"){

            RegistrationService.createRegistrations(registration).then(res => {
                this.props.history.push('/registrations');
            });

        }else{

            /*StudentService.updateStudent(student, this.state.id).then( res => {
                this.props.history.push('/students');
            });*/
        }       
    } 

    changeregistration_dateHandler = (event) => {
        this.setState({registration_date: event.target.value});
    }

    changeexamen_gradeHandler = (event) => {
        this.setState({examen_grade: event.target.value});
    }

    changeproject_gradeHandler = (event) => {
        this.setState({project_grade: event.target.value});
    }

    changelaborator_gradeHandler = (event) => {
        this.setState({laborator_grade: event.target.value});
    }

    changepresence_gradeHandler = (event) => {
        this.setState({presence_grade: event.target.value});
    }

    cancel(){
        this.props.history.push('/registrations');
    }

    getTitle(){
        if(this.state.id === "_add"){
            return <h3 className = "text-center">Add Registration</h3>
        }
    }

    render() {
        return (
            <div>
                <div className = "container">
                    <br></br>
                    <div className = "row">
                        <div className = "card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className = "card-body">
                                <form>
                                    <div className = "form-group">
                                        <label> Registration Date: </label>
                                        <input placeholder="yyyy-mm-dd" name="registration_date" className="form-control"
                                        value={this.state.registration_date} onChange={this.changeregistration_dateHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Examen Grade: </label>
                                        <input placeholder="ExamenGrade" name="examen_grade" className="form-control"
                                        value={this.state.examen_grade} onChange={this.changeexamen_gradeHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Project Grade: </label>
                                        <input placeholder="ProjectGrade" name="project_grade" className="form-control"
                                        value={this.state.project_grade} onChange={this.changeproject_gradeHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Laborator Grade: </label>
                                        <input placeholder="LaboratorGrade" name="laborator_grade" className="form-control"
                                        value={this.state.laborator_grade} onChange={this.changelaborator_gradeHandler}/>
                                    </div>

                                    <div className = "form-group">
                                        <label> Presence Grade: </label>
                                        <input placeholder="PresenceGrade" name="presence_grade" className="form-control"
                                        value={this.state.presence_grade} onChange={this.changepresence_gradeHandler}/>
                                    </div>

                                    <br></br>
                                    <button className="btn btn-success" onClick={this.SaveOrUpdateCourse}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateRegistrationComponent;