# School Management System

A Rest API for a school management system implemented with ReactJS, Spring Boot and Mysql. 

The app use CRUD operations in order to add new students, courses and registrations.

This are implemented in Spring Boot in Controller folder.

The registrations is the many to many relationship between student and course.

Once you run the app you can add new students/courses, view, delete, update the existent students/courses from frontend.

In order to create a link between a student and course, we must update the registration table from frontend. 
