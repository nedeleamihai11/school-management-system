package net.javaguides.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.model.Registration;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Long> {

}