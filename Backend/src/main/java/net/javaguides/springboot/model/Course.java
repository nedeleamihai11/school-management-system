package net.javaguides.springboot.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "courses")
public class Course {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "Name")
	private String name;
	
	@Column(name = "Duration")
    private int duration;
	
	@Column(name = "Profesor_Name")
    private String profesor_name;
	
	@Column(name = "Credit_Points")
    private int credit_points;
	
    @JsonIgnore
    @OneToMany(mappedBy = "course")
    private Set<Registration> registrations = new HashSet<>();

	public Course() {

	}

	public Course(String name, int duration, String profesor_name, int credit_points) {
		super();
		this.name = name;
		this.duration = duration;
		this.profesor_name = profesor_name;
		this.credit_points = credit_points;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getProfesor_name() {
		return profesor_name;
	}

	public void setProfesor_name(String profesor_name) {
		this.profesor_name = profesor_name;
	}

	public int getCredit_points() {
		return credit_points;
	}

	public void setCredit_points(int credit_points) {
		this.credit_points = credit_points;
	}
	
	public Set<Registration> getRegistrations() {
		return registrations;
	}

	public void setRegistrations(Set<Registration> registrations) {
		this.registrations = registrations;
	}
	
}
