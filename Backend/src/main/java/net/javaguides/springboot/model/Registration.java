package net.javaguides.springboot.model;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "registration")
public class Registration {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private Student student;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    private Course course;
	
    @JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "Registration_Date")
    private Calendar registration_date;
	
	@Column(name = "Examen_Grade")
    private int examen_grade;
	
	@Column(name = "Project_Grade")
    private int project_grade;
	
	@Column(name = "Laborator_Grade")
    private int laborator_grade;
	
	@Column(name = "Presence_Grade")
    private int presence_grade;

	public Registration() {

	}

	public Registration(Student student, Course course, int examen_grade, int project_grade, int laborator_grade,
			int presence_grade, Calendar registration_date) {
		super();
		this.student = student;
		this.course = course;
		this.examen_grade = examen_grade;
		this.project_grade = project_grade;
		this.laborator_grade = laborator_grade;
		this.presence_grade = presence_grade;
		this.registration_date = registration_date;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public int getExamen_grade() {
		return examen_grade;
	}

	public void setExamen_grade(int examen_grade) {
		this.examen_grade = examen_grade;
	}

	public int getProject_grade() {
		return project_grade;
	}

	public void setProject_grade(int project_grade) {
		this.project_grade = project_grade;
	}

	public int getLaborator_grade() {
		return laborator_grade;
	}

	public void setLaborator_grade(int laborator_grade) {
		this.laborator_grade = laborator_grade;
	}

	public int getPresence_grade() {
		return presence_grade;
	}

	public void setPresence_grade(int presence_grade) {
		this.presence_grade = presence_grade;
	}

	public Calendar getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(Calendar registration_date) {
		this.registration_date = registration_date;
	}
}
