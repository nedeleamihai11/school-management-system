package net.javaguides.springboot.model;

import javax.persistence.GeneratedValue;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "students")
public class Student {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "email_id")
	private String emaildID;
	
    @JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "Date_Birth")
    private Calendar datebirth;
	
	@Column(name = "CNP")
    private String CNP;
	
	@Column(name = "Phonenumber")
    private String phonenumber;
	
	@Column(name = "Mother_Name")
    private String mother_name;
	
	@Column(name = "Father_Name")
    private String father_name;
	
    @JsonIgnore
    @OneToMany(mappedBy = "student")
    private Set<Registration> registrations = new HashSet<>();

	public Student() {

	}
	
	public Student(String firstName, String lastName, String emaildID, Calendar datebirth, String CNP, String phonenumber, String mother_name, String father_name) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.emaildID = emaildID;
		this.datebirth = datebirth;
		this.CNP = CNP;
		this.phonenumber = phonenumber;
		this.mother_name = mother_name;
		this.father_name = father_name;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmaildID() {
		return emaildID;
	}
	
	public void setEmaildID(String emaildID) {
		this.emaildID = emaildID;
	}
	
	public Calendar getDatebirth() {
		return datebirth;
	}

	public void setDatebirth(Calendar datebirth) {
		this.datebirth = datebirth;
	}

	public String getCNP() {
		return CNP;
	}

	public void setCNP(String cNP) {
		CNP = cNP;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getMother_name() {
		return mother_name;
	}

	public void setMother_name(String mother_name) {
		this.mother_name = mother_name;
	}

	public String getFather_name() {
		return father_name;
	}

	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}

	public Set<Registration> getRegistrations() {
		return registrations;
	}

	public void setRegistrations(Set<Registration> registrations) {
		this.registrations = registrations;
	}
}
