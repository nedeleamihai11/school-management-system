package net.javaguides.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.model.Course;
import net.javaguides.springboot.model.Registration;
import net.javaguides.springboot.model.Student;
import net.javaguides.springboot.repository.CourseRepository;
import net.javaguides.springboot.repository.RegistrationRepository;
import net.javaguides.springboot.repository.StudentRepository;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class RegistrationController {
	
	@Autowired
	private RegistrationRepository registrationRepository;
	
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private CourseRepository courseRepository;
	
	@GetMapping("/registrations")
	public List<Registration> getAllRegistrations(){
		return registrationRepository.findAll();
	}
	
	// create registration rest api
	@PostMapping("/registrations")
	public Registration createRegistration(@RequestBody Registration registration) {
		return registrationRepository.save(registration);
	}
	
    @PutMapping("/registrations/{registrationId}/course/{courseId}")
    public Registration addCourseToRegistration(
            @PathVariable Long registrationId,
            @PathVariable Long courseId
    ) {
        Registration registration = registrationRepository.findById(registrationId).get();
        Course course = courseRepository.findById(courseId).get();
        registration.setCourse(course);
        return registrationRepository.save(registration);
    }
    
    @PutMapping("/registrations/{registrationId}/student/{studentId}")
    public Registration addStudentToRegistration(
            @PathVariable Long registrationId,
            @PathVariable Long studentId
    ) {
        Registration registration = registrationRepository.findById(registrationId).get();
        Student student = studentRepository.findById(studentId).get();
        registration.setStudent(student);
        return registrationRepository.save(registration);
    }
}
